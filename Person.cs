﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YellowPages
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }

        public Person(string firstName, string lastName, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            FullName = firstName + " " + lastName;
        }

        public string ToString()
        {
            return $"First name: {FirstName}\nLast name: {LastName}\nPhone number: {PhoneNumber}";
        }
    }
}
