﻿using System;
using System.Collections.Generic;

namespace YellowPages
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> contacts = new List<Person>();
            string search;
            string answer;
            bool matches;

            static void initializeContacts(List<Person> contacts)
            {
                Person contact1 = new Person("Rick", "Sanchez", "+1-202-555-0138");
                Person contact2 = new Person("Morty", "Smith", "+1-202-555-0154");
                Person contact3 = new Person("Summer", "Smith", "+1-202-555-0128");
                Person contact4 = new Person("Beth", "Smith", "+1-202-555-0127");
                Person contact5 = new Person("Jerry", "Smith", "+1-202-555-0159");

                contacts.Add(contact1);
                contacts.Add(contact2);
                contacts.Add(contact3);
                contacts.Add(contact4);
                contacts.Add(contact5);
            }

            static void searchContact(List<Person> contacts, string search, ref bool matches)
            {
                foreach (Person contact in contacts)
                {
                    if (contact.FullName.ToLower().Contains(search.ToLower()))
                    {
                        matches = true;
                        Console.WriteLine(contact.ToString());
                        Console.Write('\n');
                    }
                }

                if (!matches)
                {
                    Console.WriteLine("No matches found.");
                }
            }

            initializeContacts(contacts);

            do
            {
                matches = false;

                Console.WriteLine("Search for a persons name:");

                search = Console.ReadLine().ToLower();

                searchContact(contacts, search, ref matches);

                Console.WriteLine("Do you wish to try agian? (y, n)");

                answer = Console.ReadLine();
            }

            while (string.Equals(answer, "y"));

        }
    }
}
